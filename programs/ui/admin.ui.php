<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
bab_Widgets()->includePhpClass('Widget_Form');


class storeclient_OptionsEditor extends Widget_Form
{
	public function __construct()
	{
		$W = bab_Widgets();

		parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));


		$this->setName('options');
		$this->addClass('widget-bordered');
		$this->addClass('BabLoginMenuBackground');
		$this->addClass('widget-centered');
		$this->colon();

		$this->setCanvasOptions($this->Options()->width(60, 'em'));

		$this->addItems();
		$this->loadFormValues();

		$this->addButtons();
		$this->setSelfPageHiddenFields();
	}



    protected function addItems()
    {
        $W = bab_Widgets();

        $this->addItem(
            $W->LabelledWidget(
                storeclient_translate('Store url'),
                $W->LineEdit()->setSize(40),
                'list_url',
                storeclient_translate('This url must point to a json-encoded file with the available packages')
            )
        );


        $this->addItem(
            $W->LabelledWidget(
                storeclient_translate('Root archives folder url'),
                $W->LineEdit()->setSize(40),
                'root_url',
                storeclient_translate('Url of the base folder containing archives. If empty, the store url will be used.')
            )
        );

		/*
        $this->addItem(
            $W->LabelledWidget(
                storeclient_translate('Automatically upgrade the installed packages on a daily scheduled task'),
                $W->CheckBox(),
                'autoupgrade'
            )
        );
        */
	}


	protected function loadFormValues()
	{
		$registry = bab_getRegistry();
		$registry->changeDirectory('/bab/install_repository/');
		$values = array(
			'list_url' => (string) $registry->getValue('list_url'),
			'root_url' => (string) $registry->getValue('root_url'),
			'autoupgrade' => (bool) $registry->getValue('autoupgrade')
		);
		$this->setValues($values, array('options'));
	}


	protected function addButtons()
	{
		$W = bab_Widgets();

		$button = $W->FlowItems(
			$W->SubmitButton()
				->setAction(storeclient_Controller()->Admin()->saveConfiguration())
				->setSuccessAction(storeclient_Controller()->Admin()->configure())
				->setName('save')->setLabel(storeclient_translate('Save'))
		)->setSpacing(1, 'em');

		$this->addItem($button);
	}
}





class storeclient_InstallList {

	/**
	 * Get list of categories (filters) for the list view
	 * @return array
	 */
	private function getCategories()
	{
		$repository = bab_getInstance('bab_InstallRepository');
		/*@var $repository bab_InstallRepository */

		$categories = array();
		$arr = $repository->getFiles();
		foreach ($arr as $file) {
			$categories += $file->tags;
		}

		return $categories;
	}


	/**
	 *
	 * @param string $currentVersion
	 * @param bab_InstallRepositoryFile $recommended
	 *
	 * @return Widget_VBoxLayout
	 */
	private function recommendButton($currentVersion, bab_InstallRepositoryFile $recommended)
	{
	    $W = bab_Widgets();

	    $button = $this->postButton(
	        $recommended,
	        sprintf(
	            storeclient_translate('Upgrade to the recommended version %s'),
	            $recommended->version
	        )
	    )->addClass('widget-nowrap');

	    $c = explode('.', $currentVersion);
	    $r = explode('.', $recommended->version);

	    $sameBranch = ($c[0] === $r[0] && $c[1] === $r[1]);

	    if (!$sameBranch) {
	        return $button;
	    }


	    return $W->VBoxItems(
	        $button,
	        $W->Label(storeclient_translate('This version contains only bug fixes released since your last update'))
	    );
	}


	public function CoreFrame()
	{
		$repository = bab_getInstance('bab_InstallRepository');
		/*@var $repository bab_InstallRepository */

		$file = $repository->getLastest('ovidentia');


		if (!isset($file)) {
			return null;
		}

		$recommendedVersions = $this->getRecommendedVersions($file);
        $currentVersion = $file->getCurrentVersion();

		$W = bab_Widgets();
		$frame = $W->Frame(null, $W->HBoxLayout())
		->addClass('BabLoginMenuBackground')
		->addClass('widget-bordered')
		->addClass(Func_Icons::ICON_LEFT_24);

		$frame->addItem(
			$W->VBoxItems(
				$W->Label('Ovidentia')->addClass('widget-strong'),
				$W->Label(sprintf(storeclient_translate('Current installed version: %s'), $this->version($currentVersion))),
				$W->Label(sprintf(storeclient_translate('Last available version: %s'), $this->version($file->version)))
			)->setSizePolicy(Widget_SizePolicy::MAXIMUM)
		);


		if ($file->isInstalled() && !$file->isUpgradable()) {
			$frame->addItem($W->Label(storeclient_translate('Up to date'))->addClass('widget-nowrap'));
		} else {
			$frame->addItem(
				$links = $W->VBoxLayout()->setVerticalSpacing(2,'em')
			);

			foreach ($recommendedVersions as $recommended) {

    			if ($recommended->version != $file->version) {

    			    if (version_compare($currentVersion, $recommended->version, '<')) {
        				$links->addItem($this->recommendButton($currentVersion, $recommended));
    			    } else {
    			        $links->addItem($W->Label(
    			            sprintf(
    			                storeclient_translate('The recommended version %s is already installed'),
    			                $recommended->version
    			            )
    			        ));
    			    }
    			}

			}


			$links->addItem(
		        $this->postButton($file, storeclient_translate('Upgrade to the latest version'))
			    ->addClass('widget-nowrap')
			);
		}

		return $frame;
	}


	private function postButton($file, $label)
	{
	    $W = bab_Widgets();
	    $form = $W->Form();
	    $form->setHiddenValue('tg', bab_rp('tg'));
	    $form->setName(array('install', $file->name));

	    $form->addItem($W->Hidden(null, 'version', $file->version));
	    $form->addItem($W->Hidden(null, 'install', 1));

	    $ctrl = storeclient_Controller()->Admin();

	    $form->addItem(
	        $W->SubmitButton()
	        ->setLabel($label)
	        ->setAction($ctrl->install())
	    );

	    return $form;
	}



	private function version($version)
	{
		$c_digits = explode('.', $version);
		$beta = (int) end($c_digits);
		if ($beta >= 90) {
			$betaVersion = 1 + $c_digits[1];
			return $c_digits[0] . '.' . $betaVersion . '.0 Beta ' . ($beta - 89) . ' (' . $version . ')';
		}

		return $version;
	}


	/**
	 * Get the last version from all the patch branchs greater than current version
	 * @param array $c_digits Current version digits
	 * @return array
	 */
	private function getLastPatchBanchVersion(bab_InstallRepositoryFile $lastarchive, Array $c_digits)
	{
	    $repository = bab_getInstance('bab_InstallRepository');
	    /*@var $repository bab_InstallRepository */

	    $beta = (((int) end($c_digits)) >= 90);
	    $currentBranch = $c_digits[0].'.'.$c_digits[1];

	    $arr = $repository->getAvailableVersions($lastarchive->name);
	    $maxInBranch = array();

	    foreach ($arr as $available_version) {
	        $a_digits = explode('.', $available_version);


	        if (((int) end($a_digits)) >= 90) {
	            continue; // beta version
	        }

	        if ($a_digits[0] < $c_digits[0] || $a_digits[1] < $c_digits[1]) {
	            continue; // not the same branch
	        }

	        $branch = $a_digits[0].'.'.$a_digits[1];

	        if ($currentBranch === $branch && $beta) {
	            continue;
	        }

	        if (!isset($maxInBranch[$branch])) {
	            $maxInBranch[$branch] = '0.0.0';
	        }

	        if (version_compare($maxInBranch[$branch], $available_version, '<')) {
	            $maxInBranch[$branch] = $available_version;
	        }
	    }

	    return $maxInBranch;
	}



    /**
     * @param bab_InstallRepositoryFile $lastarchive
     * @return array
     */
	private function getRecommendedVersions(bab_InstallRepositoryFile $lastarchive)
	{
	    $repository = bab_getInstance('bab_InstallRepository');
	    /*@var $repository bab_InstallRepository */

		$current_version = $lastarchive->getCurrentVersion();

		$c_digits = explode('.', $current_version);

		if (count($c_digits) != 3) {
			return array($lastarchive);
		}

		$beta = (((int) end($c_digits)) >= 90);

		$arr = $this->getLastPatchBanchVersion($lastarchive, $c_digits);


		$files = array();

		foreach ($arr as $maxInBranch) {

		    $files[] = $repository->getFile($lastarchive->name, $maxInBranch);
		}

		return $files;
	}


	/**
	 * Checks whether the two version numbers correspond to the same branch.
	 *
	 * Note that versions with a patch number >= 80 are consider on the next minor version branch.
	 *
	 * @param string $currentVersion
	 * @param string $version
	 * @return boolean
	 */
	protected static function hasSameBranch($currentVersion, $version)
	{
	    $availableDigits = explode('.', $version);
	    $currentDigits = explode('.', $currentVersion);

	    if (count($availableDigits) !== count($currentDigits)) {
	        false;
	    }

	    $currentMajor = $currentDigits[0];
	    $availableMajor = $availableDigits[0];

	    $currentMinor = $currentDigits[1];
	    $availableMinor = $availableDigits[1];

	    $currentPatch = $currentDigits[2];
	    $availablePatch= $availableDigits[2];

	    if ($currentPatch >= 80) {
	        $currentMinor++;
	    }
	    if ($availablePatch >= 80) {
	        $availableMinor++;
	    }

	    $sameMajor = $currentMajor == $availableMajor;
	    $sameMinor = $currentMinor == $availableMinor;

	    return $sameMajor && $sameMinor;
	}


    /**
     * @return string
     */
    protected function getCurrentBranchLastVersion(Array $versions, $currentVersion)
    {
        $currentBranchVersion = $currentVersion;

        foreach ($versions as $availableVersion) {
            if (version_compare($availableVersion, $currentVersion, '<=')) {
                continue;
            }

            $sameBranch = self::hasSameBranch($currentVersion, $availableVersion);

            if (!$sameBranch) {
                continue;
            }

            if (version_compare($availableVersion, $currentBranchVersion, '<=')) {
                continue;
            }

            $currentBranchVersion = $availableVersion;
        }

        return $currentBranchVersion;
    }


    /**
     * @param bab_InstallRepositoryFile $file Last version from store
     * @param string $current_version current installed version (can be empty)
     */
    protected function currentVersion(bab_InstallRepositoryFile $file, $current_version)
    {
        $W = bab_Widgets();

        if ('' === $current_version) {
            return $W->Label('');
        }

        $addon = bab_getAddonInfosInstance($file->name);


        $link = $W->Link($current_version, '?tg=addons&idx=requirements&item='.$addon->getId());

        if (!$addon->isValid()) {
            $link->addAttribute('style', 'color:red');
        }

        return $link;
    }



    /**
     * @param bab_InstallRepositoryFile $file Last version from store
     * @param string $current_version current installed version (can be empty)
     *
     * @return Widget_Displayable_Interface
     */
    protected function proposeVersions(bab_InstallRepositoryFile $file, $current_version)
    {
        $W = bab_Widgets();

        $repository = bab_getInstance('bab_InstallRepository');
        /*@var $repository bab_InstallRepository */

        $arr = $repository->getAvailableVersions($file->name);
        $currentBranchLastVersion = $this->getCurrentBranchLastVersion($arr, $current_version);

        if ($currentBranchLastVersion === $current_version || $currentBranchLastVersion === $file->version) {
            $label = $W->Label($file->version);
            if ($title = $this->getFailedDependencies($file)) {
                $label->setTitle($title);
                $label->addAttribute('style', 'color:red');
            }
            return $W->Items($label, $W->Hidden(null, array($file->name, 'version'), $file->version));
        }

        $select = $W->Select()->setName(array($file->name, 'version'));
        $select->addOption($currentBranchLastVersion, $currentBranchLastVersion);
        $select->addOption($file->version, $file->version);

        return $select;
    }

    protected function getCurrentVersion($module)
    {
        if ('ovidentia' === $module) {
            return bab_getIniVersion();
        }

        $addon = bab_getAddonInfosInstance($module);
        if ($addon === false) {
            return '';
        }
        return $addon->getIniVersion();
    }



    protected function getFailedDependencies($file)
    {
        $dependencies = $file->dependencies;

        if (empty($dependencies)) {
            return null;
        }

        $requirements = array();
        foreach ($dependencies as $module => $rule) {
            list($operator, $requiredVersion) = $rule;
            $currentVersion = $this->getCurrentVersion($module);

            if (!version_compare($currentVersion, $requiredVersion, $operator)) {
                $requirements[] = $module.$operator.$requiredVersion;
            }
        }

        if (empty($requirements)) {
            return null;
        }

        return implode(', ', $requirements);
    }


    protected function getFiles($tag)
    {
        $repository = bab_getInstance('bab_InstallRepository');
        /*@var $repository bab_InstallRepository */


        try {
            return  $repository->getFiles($tag);


        } catch (Exception $e) {
            throw new bab_AccessException($e->getMessage());
        }
    }


    public function AddonsTableView($tag)
    {
        $W = bab_Widgets();
        $table = $W->BabTableView();
        $table->addClass('storeclient-addons-list');

        $row = 0;
        $table->addHeadRow($row);
        $table->addItem($W->Label(''), $row, 0);
        $table->addItem($W->Label(storeclient_translate('Name')), $row, 1);
        $table->addItem($W->Label(storeclient_translate('Current version')), $row, 2);
        $table->addItem($W->Label(storeclient_translate('Available version')), $row, 3);
        $table->addItem($W->Label(storeclient_translate('Install')), $row, 4);
        $row++;




        foreach ($this->getFiles($tag) as $file) {

            /*@var $file bab_InstallRepositoryFile */

            if ($file->name === 'ovidentia') {
                continue;
            }
            try {
                $current_version = (string) $file->getCurrentVersion();
            } catch (Exception $e) {
                // addon configuration in database but addonini file does not exists
                // this will allow use to restore the files as if the addon was never installed
                $current_version = '';
            }

            if (isset($file->icon)) {
                $icon = $W->Image($file->getIconUrl());
            } else {
                $icon = $W->Label('');
            }
            $table->addItem(
                $icon->setSizePolicy('widget-2em'),
                $row,
                0
            );

            $description = '';
            if (method_exists($file, 'getDescription')) {
                $description = $file->getDescription();
                $name = $W->Link($file->name, storeclient_Controller()->Admin()->display($file->name))
                        ->setOpenMode(Widget_Link::OPEN_DIALOG)
                        ->addClass('widget-strong');
            } else {
                $name = $W->Label($file->name);
            }

            $table->addItem(
                $W->VBoxItems(
                    $name,
                    $W->Label($description)
                ),
                $row,
                1
            );

            $table->addItem($this->currentVersion($file, $current_version), $row, 2);
            $table->addItem($this->proposeVersions($file, $current_version), $row, 3);

            if ($file->isInstalled() && !$file->isUpgradable()) {
                $table->addItem($W->Label(storeclient_translate('Up to date')), $row, 4);
                $table->addItem($W->Hidden(null, array($file->name, 'install'), 0), $row, 4);
            } else {
                $table->addItem($W->CheckBox()->setName(array($file->name, 'install')), $row, 4);
            }
            $row++;
        }

        return $table;
    }
}
