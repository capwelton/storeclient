<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

bab_functionality::includefile('Ovml/Container');
bab_functionality::includefile('Ovml/Function');



require_once $GLOBALS['babInstallPath'] . 'utilit/install.class.php';




/**
 * Lists the requirements of the specified version of an addon in the store.
 * If version is not specified, the latest version is used.
 *
 * <OCStoreClientAddonRequirements name="addon_name" [version="version_number"]>
 *
 * Provides the following variables:
 * <OVRequiredAddonName>
 * <OVRequiredAddonOperator>
 * <OVRequiredAddonVersion>
 */
class Func_Ovml_Container_StoreClientAddonRequirements extends Func_Ovml_Container
{

    /**
     * @var bab_InstallRepository
     */
    protected $repository = null;

    /**
     * @var array
     */
    protected $dependencies = null;

	protected $limitRows = null;
	protected $limitOffset = 0;


	/**
	 * (non-PHPdoc)
	 * @see Func_Ovml_Container::setOvmlContext()
	 */
	public function setOvmlContext(babOvTemplate $ctx)
	{
		parent::setOvmlContext($ctx);

		$this->repository = bab_getInstance('bab_InstallRepository');

		$name = $ctx->get_value('name');
		if ($name === false) {
    		$this->count = 0;
    		return;
		}

		$version = $ctx->get_value('version');

		$limit = $ctx->get_value('limit');
		if (is_string($limit)) {
			$limits = explode(',', $limit);
			if (count($limits) === 1) {
				$this->limitRows = $limit;
			} else {
				$this->limitOffset = $limits[0];
				$this->limitRows = $limits[1];
			}
		}

		$this->idx += $this->limitOffset;

		if ($version !== false) {
		    $this->file = $this->repository->getFile($name, $version);
		} else {
		    $this->file = $this->repository->getLastest($name);
		}

		$this->dependencies = array();
		$dependencies = '';
        foreach ($this->file->dependencies as $addonName => $addonRequirements) {
            $this->dependencies[] = array(
                'name' => $addonName,
                'operator' => $addonRequirements[0],
                'version' => $addonRequirements[1]
            );
        }

		$this->count = count($this->dependencies);
	}


	/**
	 * (non-PHPdoc)
	 * @see utilit/Func_Ovml_Container#getnext()
	 */
	public function getnext()
	{
		if ($this->idx >= $this->count || (isset($this->limitRows) && ($this->idx >= $this->limitRows + $this->limitOffset))) {
			$this->idx = $this->limitOffset;
			return false;
		}
		$this->ctx->curctx->push('CIndex', $this->idx);

		$dependency = $this->dependencies[$this->idx];

		$this->ctx->curctx->push('RequiredAddonName', $dependency['name']);
		$this->ctx->curctx->push('RequiredAddonOperator', $dependency['operator']);
		$this->ctx->curctx->push('RequiredAddonVersion', $dependency['version']);

		$this->idx++;
		$this->index = $this->idx;
		return true;
	}

}




/**
 * Lists the available versions of the specified addon in the store.
 * <OCStoreClientAvailableAddonVersions name="addon_name">
 *
 * Provides the following variables:
 * <OVAvailableAddonVersion>
 * <OVAvailableAddonVersionDownloadUrl>
 */
class Func_Ovml_Container_StoreClientAvailableAddonVersions extends Func_Ovml_Container
{

    /**
     * @var bab_InstallRepository
     */
    protected $repository = null;


    protected $name = null;
    /**
     * @var array
     */
    protected $versions = null;

    protected $limitRows = null;
    protected $limitOffset = 0;
	
    private static $sortKeyValue	= null;


    /**
     * (non-PHPdoc)
     * @see Func_Ovml_Container::setOvmlContext()
     */
    public function setOvmlContext(babOvTemplate $ctx)
    {
        parent::setOvmlContext($ctx);

        $this->repository = bab_getInstance('bab_InstallRepository');
        

        $this->name = $ctx->get_value('name');
        if ($this->name === false) {
            $this->count = 0;
            return;
        }

        $limit = $ctx->get_value('limit');
        if (is_string($limit)) {
            $limits = explode(',', $limit);
            if (count($limits) === 1) {
                $this->limitRows = $limit;
            } else {
                $this->limitOffset = $limits[0];
                $this->limitRows = $limits[1];
            }
        }
       

        $this->idx = $this->limitOffset;

        $versions = $this->repository->getAvailableVersions($this->name);
		$this->versions = array();
		$idx = 0;
		foreach($versions as $version) {
			$this->versions[$idx] = array('version' => $version, 'url' => '', 'date' => '0000-00-00 00:00:00');
			if ($file = $this->repository->getFile($this->name, $version)) {
	            $this->versions[$idx]['url'] = $file->geturl();
				$this->versions[$idx]['date'] = $file->date;
	        }
			$idx++;
		}
		
        $orderby = $ctx->getAttribute('orderby');
        if ($orderby && is_string($orderby)) {
			self::$sortKeyValue = $orderby;
			usort($this->versions, array('Func_Ovml_Container_StoreClientAvailableAddonVersions', 'compareArraySubValue'));
        }
		
    	$order = $ctx->getAttribute('order');
		if($order && is_string($order) && $order == "asc") {
			$this->versions = array_reverse($this->versions);
		}

        $this->count = count($this->versions);
    }


    /**
     * Compare case-insensitively two strings.
     *
     * @see bab_compare
     * @param string $string1
     * @param string $string2
     * @return int		Same values as bab_compare
     */
    private static function compareArraySubValue($obj1, $obj2)
    {
        $attr = self::$sortKeyValue;

        return bab_compare(mb_strtolower((string) $obj1[$attr]), mb_strtolower((string) $obj2[$attr]));
    }


    /**
     * (non-PHPdoc)
     * @see utilit/Func_Ovml_Container#getnext()
     */
    public function getnext()
    {
        if ($this->idx >= $this->count || (isset($this->limitRows) && ($this->idx >= $this->limitRows + $this->limitOffset))) {
            $this->idx = $this->limitOffset;
            return false;
        }
        $this->ctx->curctx->push('CIndex', $this->idx);

        $version = $this->versions[$this->count - $this->idx -1];

        $this->ctx->curctx->push('AvailableAddonVersion', $version['version']);
        $this->ctx->curctx->push('AvailableAddonVersionDownloadUrl', $version['url']);
        $this->ctx->curctx->push('AvailableAddonVersionDate', $version['date']);

        $this->idx++;
        $this->index = $this->idx;
        return true;
    }

}




/**
 * Provides information about available addons in the store.
 *
 * <OCStoreClientAddon name="">
 *
 * Provides the following variables:
 *   <OVAddonVersion>
 *   <OVAddonLicense>
 *   <OVAddonFilepath>
 *   <OVAddonDescription>
 *   <OVAddonDownloadUrl>
 *   <OVAddonIconUrl>
 *   <OVAddonImageUrl>
 *   <OVAddonLongDescriptionUrl>
 *   <OVAddonLongDescription>
 *   <OVAddonTags>
 */
class Func_Ovml_Container_StoreClientAddon extends Func_Ovml_Container
{

    /**
     * @var bab_InstallRepositoryFile
     */
    protected $addon = null;

    
    
    /**
     * (non-PHPdoc)
     * @see Func_Ovml_Container::setOvmlContext()
     */
    public function setOvmlContext(babOvTemplate $ctx)
    {
        parent::setOvmlContext($ctx);
    
        $repository = bab_getInstance('bab_InstallRepository');
        /*@var $repository bab_InstallRepository */
    
        $this->addon = $repository->getLatest($ctx->getAttribute('name'));
        $this->idx = 0;
    }
    
    /**
     * (non-PHPdoc)
     * @see utilit/Func_Ovml_Container#getnext()
     */
    public function getnext()
    {
        if ($this->idx > 0 || !isset($this->addon)) {
            $this->idx = 0;
            return false;
        }
        $this->ctx->curctx->push('CIndex', $this->idx);
    
        $file = $this->addon;
    
        $addonTags = implode(",", (array) $file->tags);
    
        $this->ctx->curctx->push('AddonDescription', $file->getDescription());
        $this->ctx->curctx->push('AddonFilepath', $file->filepath);
        $this->ctx->curctx->push('AddonVersion', $file->version);
        $this->ctx->curctx->push('AddonLicense', $file->license);
        $this->ctx->curctx->push('AddonDownloadUrl', $file->getUrl());
        $this->ctx->curctx->push('AddonIconUrl', $file->getIconUrl());
        $this->ctx->curctx->push('AddonImageUrl', $file->getImageUrl());
        $this->ctx->curctx->push('AddonLongDescriptionUrl', $file->getLongDescriptionUrl());
        $this->ctx->curctx->push('AddonLongDescription', $file->getLongDescription());
        $this->ctx->curctx->push('AddonTags', $addonTags);
    
        $this->idx++;
        $this->index = $this->idx;
        return true;
    }
}



/**
 * Provides information about available addons in the store.
 *
 * <OCStoreClientAddons>
 *
 * Provides the following variables:
 *   <OVAddonName>
 *   <OVAddonVersion>
 *   <OVAddonLicense>
 *   <OVAddonFilepath>
 *   <OVAddonDescription>
 *   <OVAddonDownloadUrl>
 *   <OVAddonIconUrl>
 *   <OVAddonImageUrl>
 *   <OVAddonLongDescriptionUrl>
 *   <OVAddonTags>
 */
class Func_Ovml_Container_StoreClientAddons extends Func_Ovml_Container
{

    /**
     * @var bab_InstallRepository
     */
    protected $repository = null;

    /**
     * @var bab_InstallRepositoryFile[]
     */
    protected $files = null;

    protected $limitRows = null;
    protected $limitOffset = 0;
	
    private static $sortKeyAttribute	= null;


    /**
     * (non-PHPdoc)
     * @see Func_Ovml_Container::setOvmlContext()
     */
    public function setOvmlContext(babOvTemplate $ctx)
    {
        parent::setOvmlContext($ctx);

        $this->repository = bab_getInstance('bab_InstallRepository');


        $limit = $ctx->getAttribute('limit');
        if (is_string($limit)) {
            $limits = explode(',', $limit);
            if (count($limits) === 1) {
                $this->limitRows = $limit;
            } else {
                $this->limitOffset = $limits[0];
                $this->limitRows = $limits[1];
            }
        }

        $this->idx += $this->limitOffset;

		

        $this->files = $this->repository->getFiles();

        $this->files = array_values($this->files);
		
        $orderby = $ctx->getAttribute('orderby');
        if (is_string($orderby)) {
        	$order = $ctx->getAttribute('order');
			
			self::$sortKeyAttribute = $orderby;
			usort($this->files, array('Func_Ovml_Container_StoreClientAddons', 'compareObjectAttribute'));
			
			if(is_string($order) && $order == "desc") {
				$this->files = array_reverse($this->files);
			}
        }

        $this->count = count($this->files);
    }


    /**
     * Compare case-insensitively two strings.
     *
     * @see bab_compare
     * @param string $string1
     * @param string $string2
     * @return int		Same values as bab_compare
     */
    private static function compareObjectAttribute($obj1, $obj2)
    {
        $attr = self::$sortKeyAttribute;

        $str1 = $obj1->$attr;
        $str2 = $obj2->$attr;

        return bab_compare(mb_strtolower((string) $str1), mb_strtolower((string) $str2));
    }
	
	

    /**
     * (non-PHPdoc)
     * @see utilit/Func_Ovml_Container#getnext()
     */
    public function getnext()
    {
        if ($this->idx >= $this->count || (isset($this->limitRows) && ($this->idx >= $this->limitRows + $this->limitOffset))) {
            $this->idx = $this->limitOffset;
            return false;
        }
        $this->ctx->curctx->push('CIndex', $this->idx);

        $file = $this->files[$this->idx];

        $longDescriptionUrl = $file->getLongDescriptionUrl();
        
        $addonTags = implode(",", (array)$file->tags);

        $this->ctx->curctx->push('AddonName', $file->name);
        $this->ctx->curctx->push('AddonDescription', $file->getDescription());
        $this->ctx->curctx->push('AddonFilepath', $file->filepath);
        $this->ctx->curctx->push('AddonVersion', $file->version);
		$this->ctx->curctx->push('AddonDate', $file->date);
        $this->ctx->curctx->push('AddonLicense', $file->license);
        $this->ctx->curctx->push('AddonDownloadUrl', $file->getUrl());
        $this->ctx->curctx->push('AddonIconUrl', $file->getIconUrl());
        $this->ctx->curctx->push('AddonImageUrl', $file->getImageUrl());
        $this->ctx->curctx->push('AddonLongDescriptionUrl', $longDescriptionUrl);
        $this->ctx->curctx->push('AddonTags', $addonTags);

        $this->idx++;
        $this->index = $this->idx;
        return true;
    }
}
