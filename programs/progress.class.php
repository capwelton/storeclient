<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'].'utilit/install.class.php';
require_once dirname(__FILE__).'/functions.php';

class storeclient_progress
{
    /**
     * @var bab_InstallRepositoryFile[]
     */
	private $packages = array();

	/**
	 *
	 * @param array $packages		The selected packages names and versions
	 */
	public function __construct(Array $packages)
	{
		$repository = bab_getInstance('bab_InstallRepository');
		/*@var $repository bab_InstallRepository */

		foreach ($packages as $name => $version) {

		    if (null === $version) {
		        $this->packages[$name] = $repository->getLastest($name);
		    } else {
			    $this->packages[$name] = $repository->getFile($name, $version);
		    }
		}
	}


	/**
	 * Process the selected packages in a progress bar
	 *
	 */
	public function processPackages()
	{
		$status = true;

		foreach ($this->packages as $name => $package) {

		    if (null === $package) {
		        bab_installWindow::message(sprintf(storeclient_translate('Package %s not found in repository'), $name));
		        $status = false;
		        continue;
		    }

		    if ($package->isInstalled() && !$package->isUpgradable()) {
		        bab_installWindow::message(sprintf(storeclient_translate('Package %s is not installable, check version number'), $package->name));
		        $status = false;
		        continue;
		    }

			if (!$package->install(true)) {
				$status = false;
			}
		}

		return $status;
	}


	public function noPackages()
	{
	    bab_installWindow::message(storeclient_translate('Nothing to install'));
	    return true;
	}
}