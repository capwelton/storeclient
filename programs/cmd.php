<?php 
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
include_once $GLOBALS['babInstallPath'].'utilit/defines.php';
require_once $GLOBALS['babInstallPath'].'utilit/utilit.php';
require_once $GLOBALS['babInstallPath'].'utilit/install.class.php';


function storeclient_cmdHelp()
{
	echo 'php -f index.php "addon=storeclient.cmd" <package-name> <version>'."\n";
	echo 'exemple to download install or upgrade widgets-1-0-49.zip from the store, use: php -f index.php "addon=storeclient.cmd" widgets 1.0.49'."\n";
	echo "Note: to update ovidentia, use ovidentia as package name\n\n";
}


$repository = bab_getInstance('bab_InstallRepository');
/*@var $repository bab_InstallRepository */


function storeclient_cmdGetName($repository)
{
	if (!isset($_SERVER["argv"][2]) || empty($_SERVER["argv"][2])) {
		echo "Error: name of the package is missing\n";
		echo storeclient_cmdHelp();
	
		$packages = $repository->getFiles();
	
		echo "Available packages are: \n";
		foreach ($packages as $p) {
			/*@var $p bab_InstallRepositoryFile */
			echo $p->name."\n";
		}
	
		die();
	}
	
	return $_SERVER["argv"][2];
}


function storeclient_cmdGetVersion($repository, $name)
{
	if (!isset($_SERVER["argv"][3]) || empty($_SERVER["argv"][3])) {
		echo "Error: version of the package is missing\n";
		echo storeclient_cmdHelp();
	
		$versions = $repository->getAvailableVersions($name);
	
		echo "Available versions are: \n";
		echo implode("\n", $versions)."\n";
		die();
	}
	
	return $_SERVER["argv"][3];
}



$name = storeclient_cmdGetName($repository);
$version = storeclient_cmdGetVersion($repository, $name);

$package = $repository->getFile($name, $version);

if (!isset($package)) {
	echo sprintf("Error: no package found for %s %s\n", $name, $version);
	echo storeclient_cmdHelp();
	die();
}

define('BAB_INSTALL_TEXT_UTF8', 1); // output install message to console

$package->install();
