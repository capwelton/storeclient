

jQuery(document).ready(function() {
	
	jQuery('.storeclient-check-installed').click(function(event) {
		
		event.preventDefault();
	
		jQuery('.storeclient-addons-list input[type=checkbox]').each(function() {
			
			var checkbox = jQuery(this);
			var current_version = jQuery.trim(checkbox.closest('td').prev().prev().text());
			
			if ('' !== current_version) {
				checkbox.prop('checked', 'checked');
			}
		});
	});
});