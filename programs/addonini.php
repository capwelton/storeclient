;<?php /*

[general]
name							="storeclient"
version							="0.4.14"
license                         ="GPL-2.0+"
addon_type						="EXTENSION"
encoding						="UTF-8"
mysql_character_set_database	="latin1"
description						="List available packages from an external source, install or upgrade addons or ovidentia"
description.fr					="Module permettant l'installation et la mise à jour de modules et noyau via un store"
long_description.fr             ="README.md"
delete							="1"
ov_version						="8.3.0"
php_version						="5.1.0"
addon_access_control			="0"
author							="Cantico"
mysql_character_set_database	="latin1,utf8"
icon							="icon.png"
configuration_page				="main&idx=admin.configure"
tags							="extension,default"

[addons]

jquery						=">=1.7.1.5"
widgets						=">=1.0.108"
LibTranslate				=">=1.12.0rc3.01"

;*/