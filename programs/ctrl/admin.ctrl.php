<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */
require_once $GLOBALS['babInstallPath'].'utilit/install.class.php';


class storeclient_CtrlAdmin extends storeclient_Controller
{
	public function displaylist($tag = null)
	{
		if (!bab_isUserAdministrator()) {
			throw new bab_AccessException(bab_translate('Access denied to non administrators'));
		}


		require_once dirname(__FILE__).'/../ui/admin.ui.php';
		$W = bab_Widgets();

		$repository = bab_getInstance('bab_InstallRepository');
		/*@var $repository bab_InstallRepository */

		$installList = new storeclient_InstallList;

		$page = $W->BabPage();
		$page->setTitle(sprintf(storeclient_translate('Packages available on %s'), $repository->getRootUrl()));

		bab_functionality::includeOriginal('Icons');

		if (!isset($tag) && $core = $installList->CoreFrame()) {
			$page->addItem($core);
		}


		$form = $W->Form();
		$form->setHiddenValue('tg', bab_rp('tg'));
		if (isset($tag)) {
		    $form->setHiddenValue('tag', $tag);
		}
		$form->setName('install');
		$page->addItem($form);
		$form->addItem($installList->AddonsTableView($tag));


		$bottom = $W->Frame(null, $W->FlowItems()->setSpacing(2, 'em')->setVerticalAlign('middle'));
		$bottom->addClass('widget-centered');
		$bottom->setCanvasOptions($bottom->options()->width(35, 'em'));

		$bottom->addItem(
		    $W->Link(storeclient_translate('Check installed addons'), $this->proxy()->displaylist())
		    ->addClass('storeclient-check-installed')
		);

		$bottom->addItem(
			$W->SubmitButton()
				->setLabel(storeclient_translate('Install selected packages'))
				->setAction($this->proxy()->install())
				->addClass('widget-centered')
		);



		$form->addItem($bottom);
		$addon = bab_getAddonInfosInstance('storeclient');
		$page->addJavascriptFile($addon->getPhpPath().'list.js');

		$babBody = bab_getBody();

		if (method_exists($babBody, 'addMenu')) {
		    // since 8.4.93
    		$babBody->addMenu('babAdminInstall');
        	$babBody->setCurrentItemMenu('storeclientList');
		}
    	bab_siteMap::setPosition('storeclientList');

		return $page;
	}



	/**
	 *
	 * @param string $name     The addon name
	 *
	 * @throws bab_AccessException
	 * @return widget_Page
	 */
    public function display($name)
    {
        if (!bab_isUserAdministrator()) {
            throw new bab_AccessException(bab_translate('Access denied to non administrators'));
        }

        require_once dirname(__FILE__).'/../ui/admin.ui.php';
        $W = bab_Widgets();

        $repository = bab_getInstance('bab_InstallRepository');
        /*@var $repository bab_InstallRepository */

        $addonFile = $repository->getLastest($name);

        $page = $W->BabPage();
        $page->setTitle($addonFile->name);

        $iconUrl = $addonFile->getIconUrl();


        $detailsSection = $W->Section(
            storeclient_translate('Details'),
            $W->VBoxItems(
                $W->FlowItems(
                    $W->Label(storeclient_translate('Version'))->setSizePolicy('widget-10em'),
                    $W->Label($addonFile->version)
                )
            )
        );
        $detailsSection->setFoldable();

        if (isset($addonFile->license)) {
            $detailsSection->addItem(
                $W->FlowItems(
                    $W->Label(storeclient_translate('License'))->setSizePolicy('widget-10em'),
                    $W->Label($addonFile->license)
                )
            );
        }

        if (isset($addonFile->author)) {
            $detailsSection->addItem(
                $W->FlowItems(
                    $W->Label(storeclient_translate('Author'))->setSizePolicy('widget-10em'),
                    $W->Label($addonFile->author)
                )
            );
        }


        $dependenciesSection = $W->Section(
            storeclient_translate('Dependencies'),
            $W->VBoxItems()
        );
        $dependenciesSection->setFoldable();

        foreach ($addonFile->dependencies as $name => $value) {
            $dependenciesSection->addItem(
                $W->FlowItems(
                    $W->Label($name)->setSizePolicy('widget-10em'),
                    $W->Label(implode(' ', $value))
                )
            );
        }

        $imageUrl = $addonFile->getImageUrl();
        $image = null;
        if (isset($imageUrl)) {
            $image = $W->Image($imageUrl);
        }

        $mainBox = $W->VBoxItems();
        $mainBox->setVerticalSpacing(2, 'em');
        $page->addItem($mainBox);

        $mainBox->addItem(
            $W->FlowItems(
                $W->Image($iconUrl)->setSizePolicy('widget-5em'),
                $W->VBoxItems(
                    $W->Title($addonFile->name, 2),
                    $W->Html(bab_toHtml($addonFile->getDescription(), BAB_HTML_ALL)),
                    $W->Html($addonFile->getLongDescription()),
                    $image,
                    $detailsSection,
                    $dependenciesSection
                )->setVerticalSpacing(1, 'em')
            )->setVerticalAlign('top')
        );


        return $page;
    }






	/**
	 * Upgrade selected items
	 */
	public function install(Array $install = null, $tag = null)
	{
	    if (method_exists($this, 'requireSaveMethod')) {
	        $this->requireSaveMethod(); // 8.4.91
	    }

	    require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
		require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';
		$packages = array();

		foreach ($install as $name => $addon) {
			if ($addon['install']) {
				$packages[$name] = $addon['version'];
			}
		}

		$session = bab_getInstance('bab_Session');
		$session->storeclient_packages = $packages;

		$frameurl = new bab_url();
		$frameurl->addon = 'storeclient.installprogress';

		bab_installWindow::getPage(
			storeclient_translate('Installation of the selected packages'),
			$frameurl->toString(),
			storeclient_translate('Continue'),
			$this->proxy()->displayList($tag)->url()
		);
	}

	/**
	 * progress bar
	 * @param	Array $install 	selected package
	 */
	public function installProgress(Array $packages = null)
	{

	}



	public function configure()
	{
		require_once dirname(__FILE__).'/../ui/admin.ui.php';

		$W = bab_Widgets();
		$page = $W->BabPage();

		$editor = new storeclient_OptionsEditor();
		$page->addItem($editor);

		return $page;

	}


	public function saveConfiguration(Array $options = null)
	{
		$registry = bab_getRegistry();
		$registry->changeDirectory('/bab/install_repository/');

		if (!isset($options['root_url']) || empty($options['root_url'])) {
			// file storage root, relative to csv file
			$options['root_url'] = $options['list_url'];
		}

		$registry->setKeyValue('list_url', $options['list_url']);
		$registry->setKeyValue('root_url', $options['root_url']);
		$registry->setKeyValue('autoupgrade', (bool) $options['autoupgrade']);

		return true;
	}
}
