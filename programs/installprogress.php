<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */


include_once $GLOBALS['babInstallPath'].'utilit/defines.php';
require_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
require_once dirname(__FILE__).'/progress.class.php';


ini_set('default_charset', bab_charset::getIso());

$session = bab_getInstance('bab_Session');
/*@var $session bab_Session */


$progress = new storeclient_progress($session->storeclient_packages);

$frame = new bab_installWindow;

if (!isset($GLOBALS['babMkdirMode'])) {
	$GLOBALS['babMkdirMode'] = 0770;
}

if (empty($session->storeclient_packages)) {
    $frame->startInstall(array($progress, 'noPackages'));
    die();
}

$frame->startInstall(array($progress, 'processPackages'));
die();